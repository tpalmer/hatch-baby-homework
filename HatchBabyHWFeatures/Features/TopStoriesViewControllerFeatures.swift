//
//  TopStoriesViewControllerFeatures.swift
//  HatchBabyHWUITests
//
//  Created by Travis Palmer on 12/30/18.
//  Copyright © 2018 Travis Palmer. All rights reserved.
//

@testable import HatchBabyHW
import Nocilla

class TopStoriesViewControllerFeatures: KIFTestCase {
    func testPresenceOfStoriesView() {
        viewTester().usingLabel("Top Stories List").waitForView()
    }
}
