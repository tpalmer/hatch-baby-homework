//
//  InitialViewControllerFeatures.swift
//  HatchBabyHWFeatures
//
//  Created by Travis Palmer on 12/30/18.
//  Copyright © 2018 Very. All rights reserved.
//

@testable import HatchBabyHW
import Nocilla

class InitialViewControllerFeatures: KIFTestCase {
    override func beforeAll() {
        super.beforeAll()

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let navigationVC = storyboard.instantiateInitialViewController() as? UINavigationController else {
            XCTAssert(false, "expected UINavigationController for initial view controller")
            return
        }
        UIApplication.shared.keyWindow!.rootViewController = navigationVC
    }

    func testInitialViewController() {
        viewTester().usingLabel("Top Stories View")?.waitForView()
    }
}
