//
//  TopStoryDetailViewControllerFeatures.swift
//  HatchBabyHWFeatures
//
//  Created by Travis Palmer on 12/31/18.
//  Copyright © 2018 Very. All rights reserved.
//

@testable import HatchBabyHW
import Nocilla

class TopStoryDetailViewControllerFeatures: KIFTestCase {
    func testPresenceOfDetailView() {
        guard let navigationVC = UIApplication.shared.keyWindow!.rootViewController as? UINavigationController else {
            XCTFail("Unable to find navigation controller")
            return
        }
        guard let topStoriesVC = navigationVC.viewControllers.first as? TopStoriesViewController else {
            XCTFail("Unable to find top stories view controller")
            return
        }

        topStoriesVC.viewDidLoad()

        viewTester().wait(forTimeInterval: 1)
        viewTester().usingLabel("Top Stories List").tapRowInTableView(at: IndexPath(row: 0, section: 0))
        viewTester().usingLabel("Top Story Detail").waitForView()
    }
}
