//
//  APIManager.swift
//  HatchBabyHW
//
//  Created by Travis Palmer on 12/30/18.
//  Copyright © 2018 Travis Palmer. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift

typealias APISuccessCallback = () -> Void
typealias APIFailureCallback = (_ errors: [String]?) -> Void

protocol APIManagerProtocol {
    static func fetchTopStories(
        success: @escaping APISuccessCallback,
        failure: @escaping APIFailureCallback
    )
}

class APIManager: APIManagerProtocol {

    struct Constants {
        static let apiKey = "aea4ca54b5cd4c98aecb7d89c3dc7a03"
        static let apiURL = "https://api.nytimes.com/svc/topstories/v2/home.json?api-key=\(apiKey)"
    }

    public class func fetchTopStories (
        success: @escaping APISuccessCallback,
        failure: @escaping APIFailureCallback) {

        guard APIManager.shouldRefreshStories else {
            return
        }

        Alamofire.request(
            "\(Constants.apiURL)"
            ).responseObject { (response: DataResponse<FetchTopStoriesResponse>) in

                switch response.result {
                case .success:
                    let topStoriesResponse = response.result.value

                    guard let topStories = topStoriesResponse?.results else {
                        let error = "Unable to map top stories from response."
                        logger.error(error)
                        failure([error])
                        return
                    }

                    logger.debug("Fetched \(topStories.count) Top Stories")

                    DispatchQueue.global(qos: .background).async {
                        // Get realm and table instances for this thread.
                        let realm = RealmManager.instance()

                        realm.beginWrite()

                        for topStory in topStories {
                            realm.add(topStory, update: true)
                        }

                        do {
                            logger.debug("Saving top stories.")
                            try realm.commitWrite()
                        } catch {
                            logger.error("Failed saving top stories!")
                        }

                        DispatchQueue.main.async {
                            let defaults = UserDefaults.standard
                            let formatter = DateFormatter()
                            formatter.dateFormat = "yyyy MM dd"
                            defaults.set(
                                formatter.string(from: Date()),
                                forKey: Helper.UserDefaultKeys.lastTopStoriesRefresh.rawValue
                            )

                            // Run any passed completion closure in the main thread.
                            success()
                        }
                    }
                case .failure(let error):
                    logger.error(error)
                    failure([error.localizedDescription])
                }
        }
    }

    internal static var shouldRefreshStories: Bool {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy MM dd"
        let defaults = UserDefaults.standard
        let lastRefresh = defaults.string(forKey: Helper.UserDefaultKeys.lastTopStoriesRefresh.rawValue)
        let lastRefreshDate = formatter.date(from: lastRefresh ?? "")

        if lastRefreshDate != nil, Calendar.current.isDateInToday(lastRefreshDate!) {
            return false
        }

        return true
    }
}
