//
//  TopStory.swift
//  HatchBabyHW
//
//  Created by Travis Palmer on 12/30/18.
//  Copyright © 2018 Travis Palmer. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
import AlamofireObjectMapper
import SwiftyJSON

class FetchTopStoriesResponse: Mappable {
    var results: [TopStory]?

    required init?(map: Map) {}

    func mapping(map: Map) {
        results <- map["results"]
    }
}

class TopStory: Object, Mappable {

    @objc dynamic var title = ""
    @objc dynamic var url = ""
    @objc dynamic var thumbnailUrl = ""
    @objc dynamic var publishedDate = ""
    @objc dynamic var keywords = ""
    @objc dynamic var leadParagraph = ""
    @objc dynamic var source = ""
    @objc dynamic var desFacet = ""
    @objc dynamic var orgFacet = ""
    @objc dynamic var perFacet = ""
    @objc dynamic var geoFacet = ""

    override static func primaryKey() -> String? {
        return "url"
    }

    required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        title <- map["title"]
        url <- map["url"]
        publishedDate <- map["published_date"]
        leadParagraph <- map["abstract"]
        source <- map["byline"]

        let json = JSON(map.JSON)
        if let multimedia = json["multimedia"].array {
            if let thumbnail = multimedia.first(where: { $0["format"] == "Standard Thumbnail" }) {
                thumbnailUrl = thumbnail["url"].string ?? ""
            }
        }
        if let designations = json["des_facet"].arrayObject as? [String] {
            desFacet = designations.joined(separator: " ")
        }
        if let organizations = json["org_facet"].arrayObject as? [String] {
            orgFacet = organizations.joined(separator: " ")
        }
        if let persons = json["per_facet"].arrayObject as? [String] {
            perFacet = persons.joined(separator: " ")
        }
        if let geographies = json["org_facet"].arrayObject as? [String] {
            geoFacet = geographies.joined(separator: " ")
        }

        keywords = [desFacet, orgFacet, perFacet, geoFacet].joined(separator: " ")
    }
}
