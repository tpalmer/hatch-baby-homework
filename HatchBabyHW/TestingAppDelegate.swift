//
//  TestingAppDelegate.swift
//  HatchBabyHW
//
//  Created by Travis Palmer on 12/30/18.
//  Copyright © 2018 Travis Palmer. All rights reserved.
//

import UIKit
import RealmSwift
@testable import HatchBabyHW
import Nocilla

class TestingAppDelegate: NSObject {
    @objc var window: UIWindow?

    @objc func application(_ application: UIApplication,
                           didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        let mainAppDelegate = AppDelegate()
        mainAppDelegate.configureRealm()

        LSNocilla.sharedInstance().start()
        self.defineGlobalStubs()

        return true
    }

    func defineGlobalStubs () {
        let imageURL = "http://static01.nyt.com/images/2014/12/03/business/Economy/Economy-thumbStandard.jpg"
        let stubbedBody = """
        {
          "results": [
            {
              "title": "New Round of Fiscal Battles in Congress Clouds the Economy",
              "abstract": "As another round of fiscal brinkmanship looms with Republican control of Congress...",
              "url": "http://www.nytimes.com/2014/12/03/business/economy/uncertainty.html",
              "byline": "By JONATHAN WEISMAN",
              "published_date": "2014-12-03T00:00:00-5:00",
              "des_facet": [
                "United States Politics and Government",
                "United States Economy",
                "Law and Legislation",
                "Shutdowns (Institutional)"
              ],
              "org_facet": [
                "Democratic Party",
                "Republican Party"
              ],
              "per_facet": [
                "Scott, Keith Lamont (1973-2016)",
                "McCrory, Pat"
              ],
              "geo_facet": [
                "North Carolina",
                "Charlotte (NC)"
              ],
              "multimedia": [
                {
                  "url": "\(imageURL)",
                  "format": "Standard Thumbnail"
                }
              ]
            }
          ]
        }
        """
        _ = stubRequest("GET",
                        "\(APIManager.Constants.apiURL)" as LSMatcheable)
            .andReturn(200)
            .withBody(stubbedBody as LSHTTPBody)

        let responseImage = UIImageJPEGRepresentation(#imageLiteral(resourceName: "egg"), 1)!
        _ = stubRequest("GET", imageURL.regex()).andReturnRawResponse(responseImage)

        stubRequest("GET", "https://static.realm.io".regex())
        stubRequest("GET", "https://api.mixpanel.com".regex())
    }
}
