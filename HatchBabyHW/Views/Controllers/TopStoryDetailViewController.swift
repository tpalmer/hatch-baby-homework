//
//  TopStoryDetailViewController.swift
//  HatchBabyHW
//
//  Created by Travis Palmer on 12/30/18.
//  Copyright © 2018 Travis Palmer. All rights reserved.
//

import UIKit
import AlamofireImage

class TopStoryDetailViewController: UIViewController {

    var topStory: TopStory?
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var publishedDate: UILabel!
    @IBOutlet var keywords: UILabel!
    @IBOutlet var leadParagraph: UILabel!
    @IBOutlet var source: UILabel!
    @IBOutlet var imageView: UIImageView!

    override func viewDidLoad() {
        self.titleLabel.text = self.topStory?.title
        self.publishedDate.text = self.topStory?.publishedDate
        self.keywords.text = self.topStory?.keywords
        self.leadParagraph.text = self.topStory?.leadParagraph
        self.source.text = self.topStory?.source

        if let url = URL(string: self.topStory?.thumbnailUrl ?? "") {
            self.imageView.af_setImage(withURL: url)
        }
    }
}
