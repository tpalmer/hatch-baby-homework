//
//  TopStoriesViewController.swift
//  HatchBabyHW
//
//  Created by Travis Palmer on 12/30/18.
//  Copyright © 2018 Travis Palmer. All rights reserved.
//

import UIKit
import RealmSwift
import AlamofireImage

class TopStoriesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    struct Constants {
        static let topStoryDetailSegueId = "topStoryDetailSegue"
    }

    enum TopStoriesViewControllerError: Error {
        case invalidThumbnail
        case invalidTopStory
    }

    @IBOutlet var tableView: UITableView!
    var topStories: Results<TopStory>?
    var selectedCellIndex = 0
    var apiManager: APIManagerProtocol = APIManager()
    var notificationToken: NotificationToken?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.accessibilityLabel = "Top Stories List"
        self.topStories = RealmManager.instance().objects(TopStory.self).sorted(byKeyPath: "publishedDate")

        notificationToken = self.topStories?.observe { [weak self] (changes: RealmCollectionChange) in
            guard let tableView = self?.tableView else { return }
            switch changes {
            case .initial:
                // Results are now populated and can be accessed without blocking the UI
                tableView.reloadData()
            case .update(_, let deletions, let insertions, let modifications):
                // Query results have changed, so apply them to the UITableView
                tableView.beginUpdates()
                tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.deleteRows(at: deletions.map({ IndexPath(row: $0, section: 0)}),
                                     with: .automatic)
                tableView.reloadRows(at: modifications.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.endUpdates()
            case .error(let error):
                // An error occurred while opening the Realm file on the background worker thread
                fatalError("\(error)")
            }
        }

        type(of: apiManager).fetchTopStories(
            success: {
                logger.info("Successfully fetched top stories")
            },
            failure: { errors in
                let joinedErrors = errors?.joined(separator: "\n")
                logger.error("Failed to fetch top stories: \(joinedErrors ?? "")")
            }
        )
    }

    deinit {
        notificationToken?.invalidate()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.topStoryDetailSegueId {
            guard let topStory = self.topStories?[self.selectedCellIndex] else {
                return
            }

            let destinationViewController = segue.destination as? TopStoryDetailViewController
            destinationViewController?.topStory = topStory
        }
    }

    func populateCell(cell: UITableViewCell, indexPath: IndexPath) throws -> UITableViewCell {

        guard let topStory = self.topStories?[indexPath.row] else {
            throw TopStoriesViewControllerError.invalidTopStory
        }

        cell.textLabel?.text = topStory.title
        cell.detailTextLabel?.text = topStory.publishedDate

        if let url = URL(string: topStory.thumbnailUrl) {

            let group = DispatchGroup()
            group.enter()

            cell.imageView?.af_setImage(withURL: url, completion: { _ in
                group.leave()
            })

            group.notify(queue: DispatchQueue.main, execute: {
                self.tableView.reloadData()
            })
        }

        return cell
    }

    // MARK: UITableView delegate methods
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        var cell: UITableViewCell = self.tableView.dequeueReusableCell(
            withIdentifier: "topStoryCell"
        )! as UITableViewCell

        do {
            try cell = populateCell(cell: cell, indexPath: indexPath)
        } catch TopStoriesViewControllerError.invalidTopStory {
            cell.imageView?.image = nil
        } catch {
            logger.error("Error populating cell: \(error)")
        }

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedCellIndex = indexPath.row
        self.performSegue(withIdentifier: Constants.topStoryDetailSegueId, sender: self)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.topStories?.count ?? 0
    }
}
