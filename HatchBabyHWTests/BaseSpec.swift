//
//  BaseSpec.swift
//  HatchBabyHW
//
//  Created by Travis Palmer on 8/20/18.
//  Copyright © 2018 Very. All rights reserved.
//

@testable import HatchBabyHW
import Quick
import Nimble
import RealmSwift
import Nocilla

class BaseSpec: QuickSpec {
    override func spec() {
        super.spec()

        beforeEach {
            let defaults = UserDefaults.standard
            defaults.removeObject(forKey: Helper.UserDefaultKeys.lastTopStoriesRefresh.rawValue)
        }
    }
}
