//
//  TopStoriesViewControllerSpecs.swift
//  HatchBabyHWTests
//
//  Created by Travis Palmer on 12/30/18.
//  Copyright © 2018 Travis Palmer. All rights reserved.
//

@testable import HatchBabyHW
import Quick
import Nimble
import RealmSwift
import Nocilla

class TopStoriesViewControllerMock: TopStoriesViewController {
    var performedSeugeIdentifier: String?

    override func performSegue(withIdentifier identifier: String, sender: Any?) {
        performedSeugeIdentifier = identifier
    }
}

class TopStoriesViewControllerSpecs: BaseSpec {
    override func spec() {
        var viewController: TopStoriesViewController!

        beforeEach {
            viewController = UIStoryboard(
                name: "Main",
                bundle: nil).instantiateViewController(
                    withIdentifier: "TopStoriesViewController"
                ) as? TopStoriesViewController

            // IBOutlets are nil unless loadView() is called.
            viewController.loadView()

            let realm = RealmManager.instance()
            RealmManager.write {
                realm.deleteAll()
                realm.create(TopStory.self, value: [
                    "url": "http://storyone",
                    "title": "test story",
                    "publishedDate": "12-21-83",
                    "thumbnailUrl": "http://test.com"
                ])
                realm.create(TopStory.self, value: [
                    "url": "http://storytwo",
                    "title": "test story 2",
                    "publishedDate": "12-21-84",
                    "thumbnailUrl": "http://test.com"
                ])
                realm.create(TopStory.self, value: [
                    "url": "http://storythree",
                    "title": "test story 3",
                    "publishedDate": "12-21-85",
                    "thumbnailUrl": "http://test.com"
                ])
            }
        }

        describe("viewDidLoad") {
            beforeEach {
                viewController.apiManager = APIManagerProtocolMock()
                viewController.viewDidLoad()
            }

            afterEach {
                APIManagerProtocolMock.resetState()
            }

            it("initializes top stories") {
                expect(viewController.topStories?.count).to(equal(3))
            }

            it("sets the tableview accessibility label") {
                expect(viewController.tableView.accessibilityLabel).to(equal("Top Stories List"))
            }

            it("calls fetchTopStories") {
                expect(APIManagerProtocolMock.fetchStoriesCallCount).to(equal(1))
            }
        }

        describe("prepareForSegue") {
            it("sets the top story on the destination view controller") {
                let destinationVC = UIStoryboard(
                    name: "Main",
                    bundle: nil).instantiateViewController(
                        withIdentifier: "TopStoryDetailViewController"
                    ) as? TopStoryDetailViewController

                let segue = UIStoryboardSegue(
                    identifier: TopStoriesViewController.Constants.topStoryDetailSegueId,
                    source: viewController,
                    destination: destinationVC!
                )

                viewController.viewDidLoad()
                viewController.prepare(for: segue, sender: viewController)

                expect(destinationVC?.topStory).toNot(beNil())
            }
        }

        describe("populateCell") {
            var cell: UITableViewCell!

            beforeEach {
                viewController.viewDidLoad()

                let responseImage = UIImageJPEGRepresentation(#imageLiteral(resourceName: "egg"), 1)!
                _ = stubRequest("GET", "http://test.com".regex()).andReturnRawResponse(responseImage)

                cell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "topCell")

                do {
                    try cell = viewController.populateCell(
                        cell: cell,
                        indexPath: IndexPath(row: 0, section: 0)
                    )
                } catch {
                    XCTFail("Unable to populate cell")
                }
            }

            it("sets the text label") {
                expect(cell.textLabel?.text).to(equal("test story"))
            }

            it("sets the detail text label") {
                expect(cell.detailTextLabel?.text).to(equal("12-21-83"))
            }

            it("sets the thumbnail image") {
                expect(cell.imageView?.image).toEventuallyNot(beNil())
            }
        }

        context("UITableViewDelegate methods") {
            describe("didSelectRowAtIndexPath") {
                beforeEach {
                    viewController.viewDidLoad()
                }

                it("sets the selectedCellIndex") {
                    viewController.tableView(
                        viewController.tableView,
                        didSelectRowAt: IndexPath(row: 2, section: 0)
                    )

                    expect(viewController.selectedCellIndex).to(equal(2))
                }

                it("performs a segue to the detail view") {
                    let topStoriesVCMock = TopStoriesViewControllerMock()

                    topStoriesVCMock.tableView(
                        UITableView(),
                        didSelectRowAt: IndexPath(row: 2, section: 0)
                    )

                    expect(topStoriesVCMock.performedSeugeIdentifier).to(
                        equal(TopStoriesViewController.Constants.topStoryDetailSegueId)
                    )
                }
            }
        }
    }
}
