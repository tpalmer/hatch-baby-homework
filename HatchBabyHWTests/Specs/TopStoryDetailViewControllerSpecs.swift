//
//  TopStoryDetailViewControllerSpecs.swift
//  HatchBabyHWTests
//
//  Created by Travis Palmer on 12/31/18.
//  Copyright © 2018 Very. All rights reserved.
//

@testable import HatchBabyHW
import Quick
import Nimble
import RealmSwift
import Nocilla

class TopStoryDetailViewControllerSpecs: BaseSpec {
    override func spec() {
        var viewController: TopStoryDetailViewController!

        beforeEach {
            viewController = UIStoryboard(
                name: "Main",
                bundle: nil).instantiateViewController(
                    withIdentifier: "TopStoryDetailViewController"
                ) as? TopStoryDetailViewController

            // IBOutlets are nil unless loadView() is called.
            viewController.loadView()

            let topStory = TopStory(value: [
                "url": "http://storyone",
                "title": "test story",
                "publishedDate": "12-21-83",
                "thumbnailUrl": "http://test.com",
                "keywords": "test keyword another keyword",
                "leadParagraph": "a delightful test story",
                "source": "by the test suite"
            ])

            viewController.topStory = topStory

            let responseImage = UIImageJPEGRepresentation(#imageLiteral(resourceName: "egg"), 1)!
            _ = stubRequest("GET", "http://test.com".regex()).andReturnRawResponse(responseImage)
        }

        describe("viewDidLoad") {
            beforeEach {
                viewController.viewDidLoad()
            }

            it("sets the title label") {
                expect(viewController.titleLabel.text).to(equal("test story"))
            }

            it("sets the published date label") {
                expect(viewController.publishedDate.text).to(equal("12-21-83"))
            }

            it("sets the keywords label") {
                expect(viewController.keywords.text).to(equal("test keyword another keyword"))
            }

            it("sets the lead paragraph label") {
                expect(viewController.leadParagraph.text).to(equal("a delightful test story"))
            }

            it("sets the source label") {
                expect(viewController.source.text).to(equal("by the test suite"))
            }

            it("sets the thumbnail image") {
                expect(viewController.imageView.image).toEventuallyNot(beNil())
            }
        }
    }
}
