//
//  APIManagerSpecs.swift
//  HatchBabyHWTests
//
//  Created by Travis Palmer on 12/31/18.
//  Copyright © 2018 Very. All rights reserved.
//

@testable import HatchBabyHW
import Quick
import Nimble
import RealmSwift
import Nocilla

class APIManagerSpecs: BaseSpec {
    override func spec() {
        super.spec()

        describe("fetchTopStories") {
            it("persists top story information") {
                let realm = RealmManager.instance()

                waitUntil { done in
                    APIManager.fetchTopStories(
                        success: {
                            realm.refresh()
                            if let topStory = realm.objects(TopStory.self).first {
                                expect(topStory.url).to(
                                    equal("http://www.nytimes.com/2014/12/03/business/economy/uncertainty.html")
                                )
                                expect(topStory.publishedDate).to(equal("2014-12-03T00:00:00-5:00"))
                                expect(topStory.title).to(
                                    equal("New Round of Fiscal Battles in Congress Clouds the Economy")
                                )
                            } else {
                                XCTAssert(false, "unable to find top story.")
                            }

                            done()
                        },
                        failure: {_ in
                            XCTAssert(false, "unable to fetch top story.")
                            done()
                        }
                    )
                }
            }

            it("handles a 500 response") {
                _ = stubRequest("GET",
                                "\(APIManager.Constants.apiURL)" as LSMatcheable)
                    .andReturn(500)

                waitUntil { done in
                    APIManager.fetchTopStories(
                        success: {
                            XCTAssert(false, "fetched a top story that shouldn't exist.")
                            done()
                        },
                        failure: {_ in
                            XCTAssert(true, "successfully handled a 500 response")
                            done()
                        }
                    )
                }
            }
        }

        describe("shouldRefreshStories") {
            let defaults = UserDefaults.standard
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy MM dd"

            it("returns true if last refresh date is not today") {
                let futureDate = Calendar.current.date(byAdding: .day, value: 2, to: Date())!
                defaults.set(
                    formatter.string(from: futureDate),
                    forKey: Helper.UserDefaultKeys.lastTopStoriesRefresh.rawValue
                )

                expect(APIManager.shouldRefreshStories).to(beTrue())
            }

            it("returns false if last refresh date is today") {
                let date = Date()
                defaults.set(
                    formatter.string(from: date),
                    forKey: Helper.UserDefaultKeys.lastTopStoriesRefresh.rawValue
                )

                expect(APIManager.shouldRefreshStories).to(beFalse())
            }
        }
    }
}
