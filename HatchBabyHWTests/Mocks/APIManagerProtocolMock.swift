//
//  APIManagerProtocolMock.swift
//  HatchBabyHWTests
//
//  Created by Travis Palmer on 12/31/18.
//  Copyright © 2018 Travis Palmer. All rights reserved.
//

@testable import HatchBabyHW

class APIManagerProtocolMock: APIManagerProtocol {
    static var fetchStoriesCallCount = 0
    static var fetchStoriesShouldCallSuccess = false
    static var fetchStoriesShouldCallFailure = false

    static func fetchTopStories(success: @escaping APISuccessCallback, failure: @escaping APIFailureCallback) {
        self.fetchStoriesCallCount += 1

        if self.fetchStoriesShouldCallSuccess {
            success()
        }

        if self.fetchStoriesShouldCallFailure {
            failure(nil)
        }
    }

    public class func resetState () {
        self.fetchStoriesCallCount = 0
        self.fetchStoriesShouldCallSuccess = false
        self.fetchStoriesShouldCallFailure = false
    }
}
